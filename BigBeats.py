#!/usr/bin/env python3

import os
from functools import partial
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox as messagebox
from tkinter.ttk import *

import Icon
import SongParameters
from Playlist import *
from SongParameters import *


def hideParamters(boolVar, paramFrames):
    i = 0
    for paramFrame in paramFrames:
        if not boolVar.get():
            paramFrame.grid(sticky=EW, row=i, column=2)
            i+=1
        else:
            paramFrame.grid_forget()


class MainWindow(Tk):

    def UpdateStatusBar(self):
        """Update the bgm.txt path and prefix display at bottom of window"""
        self.statusText.set(f"File: [{self.path}]\nPrefix: [{SongParameters.prefix}]")

    def cb_Open(self):
        """Load a playlist from file"""
        path = filedialog.askopenfilename()
        if not path:
            return
        self.UpdateStatusBar()
        self.Open(path)

    def cb_Save(self):
        """Save the playlist to the current file"""
        if not self.path:
            self.cb_SaveAs()
            return
        self.SaveTo(self.path)

    def cb_SaveAs(self):
        """save the playlist to a new file"""
        path = filedialog.asksaveasfilename()
        if not path:
            return False
        self.path = path
        self.cb_Save()
        return True

    def cb_Prefix(self):
        """Ask user to update prefix"""
        d = filedialog.askdirectory()
        if not d:
            return False
        SongParameters.prefix = os.path.abspath(d)
        self.UpdateStatusBar()
        
    def cb_Help(self):
        textLink = "manual.txt"
        videoLink = "Coming soon (tm)"
        sourceLink = "https://gitlab.com/PCC_Maqo/dfci-playlist-editor"
        atText = "@PCC_Maqo on Twitter, @Maqo in the DFCI Discords"
        mb = messagebox.showinfo("Help",
        message=f"Text manual: {textLink}\nVideo guide: {videoLink}\nSource code: {sourceLink}\n{atText}"
        )
        

    def __init__(self):
        super().__init__()
        SongParameters.tk = self
        self.wm_title("DFCI Playlist Editor")
        self.geometry('520x720')

        # Top menu
        menu = Menu(self)
        self.config(menu=menu)

        # Allow canvas containing songs to fill window
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        menu.add_command(label="Save as...", command=self.cb_SaveAs, underline=5)
        menu.add_command(label="Save", command=self.cb_Save, underline=0)
        menu.add_command(label="Open...", command=self.cb_Open, underline=0)
        menu.add_command(label="Change global prefix...", command=self.cb_Prefix, underline=15)
        menu.add_command(label="Help", command=self.cb_Help, underline=0)

        # Song category tab
        self.notebook = Notebook(self)
        self.notebook.grid(column=0, row=0, sticky='NEWS')
        self.Clear()
        self.path = None

        self.statusText = StringVar(self, value="Welcome. Please open bgm.txt")
        l = Label(self, textvariable=self.statusText, relief=SUNKEN)
        l.grid(column=0, row=1, sticky='EW')

    def Clear(self):
        """Remove all songs from all notebooks"""
        self.secondFrames = []
        self.canvases = []
        if self.notebook:
            while len(ts := self.notebook.tabs()) > 0:
                self.notebook.forget(ts[0])
    
    def SaveTo(self, path):
        """Save playlist to the given file in bgm.txt format"""
        self.path = path
        SaveSongs(path, self.songs)

    def Open(self, path):
        """Open an existing bgm.txt"""
        self.path = path
        SongParameters.prefix = os.path.dirname(path)
        self.UpdateStatusBar()
        self.Clear()
        songs = GenerateSongs(path)
        self.songs = songs

        for mt in musicTypes:
            # Create a main frame
            mainFrame = Frame()
            self.notebook.add(mainFrame, text=mt)

            # Create a canvas
            canvas = Canvas(mainFrame)
            canvas.grid(row=0,column=0, sticky='NEWS')
            mainFrame.grid_columnconfigure(0, weight=1)
            mainFrame.rowconfigure(0, weight=1)
            
            # Add a scrollbar to the canvas
            scroll = Scrollbar(mainFrame, command=canvas.yview)
            scroll.grid(row=0, column=1, sticky='NS')

            # Configure canvas
            canvas.configure(scrollregion=canvas.bbox("all"))

            # Create another frame inside the canvas
            secondFrame = Frame(canvas)
            secondFrame.bind('<Configure>', self._on_frame_configure)
            self.secondFrames.append(secondFrame)
            # add that new frame to a window in the canvas
            canvas.create_window((0,0), window=secondFrame, anchor="nw")
            # When the canvas moves, set the scrollbar position/size
            canvas.configure(yscrollcommand=scroll.set)
            self.canvases.append(canvas)
        
        for i,song in enumerate(songs):
            # This song holds the frames of each song in a category
            secondFrame = self.secondFrames[song.musicType-1]

            songFrame = LabelFrame(secondFrame, text=song.File.value.get())
            songFrame.grid(column=0, sticky='EW')

            paramFrames = []
            
            # Option to hide song
            checkVar = BooleanVar()
            check = Checkbutton(songFrame, variable=checkVar, text="hide", command= partial(hideParamters, checkVar, paramFrames))
            check.grid(column=0, row=0, rowspan=5)
            sep = Separator(songFrame, orient=VERTICAL)
            sep.grid(column=1, row=0, sticky="NS", rowspan=5)


            for j, para in enumerate(song.parameters):
                paraWidget = para.MakeWidget(songFrame)
                paraWidget.grid(column=2, row=j, sticky='EW')
                paramFrames.append(paraWidget)

        self.UpdateStatusBar()

    def _on_frame_configure(self, event=None):
        """When the main window resizes, update the canvas scroll region based on the available space"""
        for canvas in self.canvases:
            x1, y1, x2, y2 = canvas.bbox("all")
            height = canvas.winfo_height()
            width = canvas.winfo_width()
            bbox = [0,0, max(x2, width), max(y2, height)]

            canvas.configure(scrollregion=bbox)

def main():
    root = MainWindow()
    Icon.ApplyIcon(root)
    root.mainloop()

if __name__ == "__main__":
    main()
