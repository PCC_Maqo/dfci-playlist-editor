"""
BSD 2-Clause License

Copyright (c) 2020, Mengxun Li
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# Modified: Minor visual tweaks and convenience functions added 2020 December, 2021 January.

from tkinter import *
from tkinter.ttk import *

from functools import partial

class MultiSlider(Frame):
    LINE_COLOR = "#476b6b"
    LINE_WIDTH = 3
    BAR_COLOR_INNER = "#5c8a8a"
    BAR_COLOR_OUTTER = "#c2d6d6"
    BAR_RADIUS = 10
    BAR_RADIUS_INNER = BAR_RADIUS-5
    DIGIT_PRECISION = '.5f' # for showing in the canvas
    HORIZONTAL_PAD = 25

    listeners = set()
    def AlertChange(self):
        for callback in self.listeners:
            callback[1]()
            # print("Called back")
        pass

    def Subscribe(self, subscriber, callback):
        self.listeners.add((subscriber, partial(callback)))

    def Unsubscribe(self, subscriber):
        subscriptions = list(self.listeners)
        result = filter(lambda x: x[0] == subscriber, subscriptions)
        for x in result:
            self.listeners.remove(x)

    def __init__(self, master, width = 400, height = 80, min_val = 0, max_val = 1, init_lis = None, show_value = True):
        Frame.__init__(self, master, height = height, width = width)
        self.master = master
        self.master.last_changed_bar = None
        if init_lis == None:
            init_lis = [min_val]
        self.init_lis = init_lis
        self.max_val = max_val
        self.min_val = min_val
        self.show_value = show_value
        self.H = height
        self.W = width
        self.canv_H = self.H
        self.canv_W = self.W
        if show_value:
            self.slider_y = self.canv_H*2/5
        else:
            self.slider_y = self.canv_H/2 # y pos of the slider
        self.slider_x = MultiSlider.BAR_RADIUS # x pos of the slider (left side)
        self.slider_x += self.HORIZONTAL_PAD

        self.bars = []
        self.last_changed_idx = None
        self.selected_idx = None # current selection bar index
        for value in self.init_lis:
            pos = (value-min_val)/(max_val-min_val)
            ids = []
            bar = {"Pos":pos, "Ids":ids, "Value":value}
            self.bars.append(bar)


        self.canv = Canvas(self, height = self.canv_H, width = self.canv_W)
        self.canv.pack()#expand=True, fill=X)
        self.canv.bind("<Motion>", self._mouseMotion)
        self.canv.bind("<B1-Motion>", self._moveBar)

        self.__addTrack(self.slider_x, self.slider_y, self.canv_W-self.slider_x, self.slider_y)
        for idx, bar in enumerate(self.bars):
            bar["Ids"] = self.__addBar(bar["Pos"], idx)

    def ChangeWidth(self, newWidth):
        self.canv_W = self.W = newWidth - self.HORIZONTAL_PAD
        self.canv.configure(width = self.canv_W)
        self.canv.delete("all")
        # self.canv = Canvas(self, height = self.canv_H, width = self.canv_W)
        self.canv.pack()
        # self.canv.bind("<Motion>", self._mouseMotion)
        # self.canv.bind("<B1-Motion>", self._moveBar)
        self.__addTrack(self.slider_x, self.slider_y, self.canv_W-self.slider_x, self.slider_y)
        # iterate over current tracks? They're built in canvas lines.
        for idx, bar in enumerate(self.bars):
            bar["Ids"] = self.__addBar(bar["Pos"], idx)


    def GetBarIndex(self, value):
        """Find index of bar with given value. Returns None if not found."""
        values = [bar["Value"] for bar in self.bars]
        index = None
        try:
            index = values.index(value)
        except ValueError:
            index = None
        return index

    def MoveLowest(self, value):
        """Set lowest value bar to a new value."""
        lowest = self.getValues()[0]
        index = self.GetBarIndex(lowest)
        self.MoveBar(index, value)

    def MoveHighest(self, value):
        """Set highest value bar to a new value."""
        highest = self.getValues()[-1]
        index = self.GetBarIndex(highest)
        self.MoveBar(index, value)

    def getValues(self):
        """Get sorted values of bars in ascending order."""
        values = [bar["Value"] for bar in self.bars]
        return sorted(values)

    def _mouseMotion(self, event):
        x = event.x
        y = event.y
        selection = self.__checkSelection(x,y)
        if selection[0]:
            self.canv.config(cursor = "hand2")
            self.selected_idx = selection[1]
            self.last_changed_idx = selection[1]
            self.master.last_changed_bar = self
        else:
            self.canv.config(cursor = "")
            self.selected_idx = None

    def KeyboardTweak(self, tweak):
        """Increment the most recent bar by a small value"""
        # assert tweak != None
        self.IncrementBar(self.last_changed_idx, tweak)

    def _moveBar(self, event):
        x = event.x
        y = event.y
        if self.selected_idx == None:
            return False
        pos = self.__calcPos(x)
        idx = self.selected_idx
        self.__moveBar(idx,pos)

    def __addTrack(self, startx, starty, endx, endy):
        id1 = self.canv.create_line(startx, starty, endx, endy, fill = MultiSlider.LINE_COLOR, width = MultiSlider.LINE_WIDTH)
        return id

    def __addBar(self, pos, idx):
        """@ pos: position of the bar, ranged from (0,1)"""
        if pos <0 or pos >1:
            raise Exception("Pos error - Pos: "+str(pos))
        R = MultiSlider.BAR_RADIUS
        r = MultiSlider.BAR_RADIUS_INNER
        L = self.canv_W - 2*self.slider_x
        y = self.slider_y
        x = self.slider_x+pos*L
        id_outer = self.canv.create_oval(x-R,y-R,x+R,y+R, fill = MultiSlider.BAR_COLOR_OUTTER, width = 2, outline = "")
        id_inner = self.canv.create_oval(x-r,y-r,x+r,y+r, fill = MultiSlider.BAR_COLOR_INNER, outline = "")
        if self.show_value:
            y_value = y+MultiSlider.BAR_RADIUS+8
            value = pos*(self.max_val - self.min_val)+self.min_val
            id_value = self.canv.create_text(x,y_value + idx*12, text = format(value, MultiSlider.DIGIT_PRECISION))
            return [id_outer, id_inner, id_value]
        else:
            return [id_outer, id_inner]

    def MoveBar(self, idx, value):
        """Move bar to value within range [self.min_val, self.max_val]"""
        # assert value >= self.min_val and value <= self.max_val
        if value < self.min_val:
            value = self.min_val
        elif value > self.max_val:
            value = self.max_val
            
        pos = (value-self.min_val)/(self.max_val-self.min_val)
        self.__moveBar(idx, pos)

    def IncrementBar(self, idx, inc):
        value = inc + self.bars[idx]["Value"]
        self.MoveBar(idx, value)

    def __moveBar(self, idx, pos):
        ids = self.bars[idx]["Ids"]
        for id in ids:
            self.canv.delete(id)
        self.bars[idx]["Ids"] = self.__addBar(pos, idx)
        self.bars[idx]["Pos"] = pos
        self.bars[idx]["Value"] = pos*(self.max_val - self.min_val)+self.min_val
        self.AlertChange()

    def __calcPos(self, x):
        """calculate position from x coordinate"""
        pos = (x - self.slider_x)/(self.canv_W-2*self.slider_x)
        if pos<0:
            return 0
        elif pos>1:
            return 1
        else:
            return pos


    def __checkSelection(self, x, y):
        """
        To check if the position is inside the bounding rectangle of a Bar
        Return [True, bar_index] or [False, None]
        """
        for idx in range(len(self.bars)):
            id = self.bars[idx]["Ids"][0]
            bbox = self.canv.bbox(id)
            if bbox[0] < x and bbox[2] > x and bbox[1] < y and bbox[3] > y:
                return [True, idx]
        return [False, None]
