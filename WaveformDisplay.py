import itertools
import threading
import tkinter as tk
# import playsound
import winsound
import wave
from time import sleep, time
from tkinter import *
from tkinter import messagebox as messagebox
from tkinter.scrolledtext import ScrolledText as ScrolledText
from tkinter.ttk import *

import numpy as np
import soundfile

import SongParameters
import Validation
from multislide import MultiSlider

# settings must be a user written module
try:
	from settings import settings
except:
	from defaultSettings import settings

FILETYPE = ".ogg"
DEFAULT_VISUAL_HALFTIME = 0.1

class WaveformDisplay(Canvas):

	@property
	def x(self):
		return self.parent.x

	@property
	def rate(self):
		return self.parent.rate

	# TODO: Probably wanna break the waveform visual element out into its own file
	def Update(self, e=None):
		"""Update the preview waveform"""
		va:Canvas = self
		assert va != None

		width = va.winfo_width()
		height = va.winfo_height()
		if width <= 1:
			return "Too small"

		# e will be true if this is called from a <configure> event; unless we were resized, don't bother
		if e and width == self.oldWidth and height == self.oldHeight:
			return

		self.oldWidth = width
		self.oldHeight = height
		
		path = self.parent.path + FILETYPE
		x, rate = self.x, self.rate
		# maxVal = self.oldMax
		maxVal = self.parent.max

		# if path != self.parent.oldPath:
			# Get the maxmimum height/depth of the sound file 
			# maxVal = max(np.amax(x), abs(np.amin(x)))
		self.parent.oldPath = path
		# self.oldMax = maxVal
		self.oldRate = rate
		self.oldX = x

		# Just look at one channel; it's probably enough
		x = x if x.ndim == 1 else x[:, 0]

		va.delete("all")

		halftime = int(self.visualHalftime * rate)
		start, end = self.parent.loopSlider.getValues()
		start += self.xOffset
		end += self.xOffset

		va.create_rectangle(0, 0, width,  height, fill=settings["backgroundColor"])

		va.create_line(width/2, 0, width/2, height, fill=settings["axisColor"])
		va.create_line(0, height/2, width, height/2, fill=settings["axisColor"])

		self.labelStart['text'] = f"-{round(halftime/rate, 5)}"
		t1 = str(round(end, 5))
		t2 = str(round(start, 5))
		# TODO: There's prooobably a cleaner way to do this.
		while len(t1) < len(t2):
			t1 = " " + t1
		while len(t2) < len(t1):
			t2 = t2 + " "
		self.labelMid['text'] = t1 + "  " + t2
		self.labelEnd['text'] = f"+{round(halftime/rate, 5)}"

		self.labelStart.place(relx=0.0, rely=0, anchor=NW, x=5, y=5)
		self.labelMid.place(relx=0.5,   rely=0, anchor=N,  x=0, y=5)
		self.labelEnd.place(relx=1.0,   rely=0, anchor=NE, x=-5, y=5)

		start, end = int(rate * start), int(rate*end)
		offset = self.xOffset
		# When correctly aligned and at 0 offset, start and ghost should be very similar to each other
		loopDemoStart = x[end-halftime:end]
		loopDemoGhost = x[start-halftime:start]
		loopDemoEnd =   x[start:start+halftime]

		loopDemo = np.concatenate((loopDemoStart, loopDemoEnd))
		sampleSize = len(loopDemo)

		color = settings["lineColor"]

		# At most, we only want 2 samples per pixel column
		skip = int(sampleSize/(2*width))
		if skip <= 0:
			skip = 1
		skip = 1

		points = []
		for i, sample in enumerate(loopDemo[::skip]):
			x = skip * width * i / len(loopDemo)
			y = (height/2) - ((sample/maxVal) * (height/2) * self.yScale)
			points.extend((x, y))
		if len(points) > 0:
			va.create_line(*points, fill=color, width=0)

		color = settings["ghostColor"]
		points = []
		for i, sample in enumerate(loopDemoGhost[::skip]):
			x = skip * width/2 * i / len(loopDemoGhost)
			y = (height/2) - ((sample/maxVal) * (height/2) * self.yScale)
			points.extend((x, y))
		if len(points) > 0:
			va.create_line(*points, fill=color, width=0)

	def changeScale(self, inc):
			self.yScale += inc
			if self.yScale < 0:
				self.yScale = 0
			self.Update()

	# TODO: FIXME: There's a crash here i think.
	def changeVisualHalftime(self, inc, set=False):
		if set:
			self.xOffset = inc
			print("Setting")
		else:
			self.visualHalftime += inc
			if self.visualHalftime < 0:
				self.visualHalftime = 0.01
		self.Update()
	
	def cb_OnScroll(self, e):
		x, y = self.winfo_pointerxy()
		if self.winfo_containing(x, y) == self:
			self.changeVisualHalftime(e.delta / 1000)
		pass

	downx = None
	xOffset = 0
	def cb_MouseDown(self, e):
		x, y = self.winfo_pointerxy()
		if self.winfo_containing(x, y) == self:
			self.downx = e.x
			print("downx", self.downx)

	def cb_MouseUp(self, e):
		self.downx = None

	def cb_Drag(self, e):
		if self.downx != None:
			# In pixels
			delta = e.x - self.downx
			self.downx = e.x
			# Convert to seconds
			scale = -2 * self.visualHalftime / self.winfo_width()
			delta *= scale

			self.xOffset += delta 
			self.Update()
		pass

	def SetLabels(self, t1, t2, t3):
		self.labelStart['text'] = t1
		self.labelMid['text'] = t2
		self.labelEnd['text'] = t3

	def __init__(self, parent):
		self.parent = parent
		super().__init__(parent)
		self.bind("<Alt-Right>", lambda _: self.changeVisualHalftime(0.001))
		self.bind("<Alt-Left>", lambda _: self.changeVisualHalftime(-0.001))

		self.bind("<Up>", lambda _:   self.changeScale(0.1))
		self.bind("<Down>", lambda _: self.changeScale(-0.1))

		self.bind("<MouseWheel>", self.cb_OnScroll) # Scroll
		self.bind("<B1-Motion>", self.cb_Drag) # Left drag
		self.bind("<ButtonPress-1>", self.cb_MouseDown)
		self.bind("<ButtonRelease-1>", self.cb_MouseUp)
		self.bind("<ButtonPress-3>", lambda _: self.changeVisualHalftime(DEFAULT_VISUAL_HALFTIME, True))

		self.labelStart = tk.Label(self, text="label1", fg="white", bg=settings["backgroundColor"], font='TkFixedFont')
		self.labelMid = tk.Label(self, text="label2", fg="white"  , bg=settings["backgroundColor"], font='TkFixedFont')
		self.labelEnd = tk.Label(self, text="label3", fg="white"  , bg=settings["backgroundColor"], font='TkFixedFont')

		self.oldWidth = 0
		self.oldHeight = 0

		self.visualHalftime = DEFAULT_VISUAL_HALFTIME # Seconds
		self.yScale = 1