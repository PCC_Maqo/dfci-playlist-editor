#!/usr/bin/env python3
#####
# Looping based on work from Joseph Ernest:
# https://afewthingz.com/seamlessaudiolooping

import itertools
import threading
import tkinter as tk
# import playsound
import winsound
import wave
from time import sleep, time
from tkinter import *
from tkinter import messagebox as messagebox
from tkinter.scrolledtext import ScrolledText as ScrolledText
from tkinter.ttk import *

import numpy as np
import soundfile

import SongParameters
import Validation
from multislide import MultiSlider
from WaveformDisplay import WaveformDisplay, FILETYPE

# settings must be a user written module
try:
	from settings import settings
except:
	from defaultSettings import settings

PREVIEW_NAME = "dfcipe_preview.wav"
PREVIEW_HALFTIME = 4 # Seconds on each side of audio loop preview

UPDATE_TIME = 31
YES = "yes"
NO = "no"
# Maximum label width
COL_1_CHAR_WIDTH = len("Start Wiggle")-2

def improveloop(x0, a, b, rate, w1, w2):
	"""
	Input:  (a, b) is a loop
			rate is the samplerate
			w1, w2 are the time before a and b that also should match up
	Output: (a, B) is a better loop 
			distance (the less the distance the better the loop)
	This function moves the loop's endpoint b to B (up to 100 ms further) such that (a, B) is a "better" loop, i.e. sum((x0[a:a+10ms] - x0[B:B+10ms])^2) is minimal
	"""

	# Convert w1, w2 from seconds to samples
	W1 = int(w1*rate)
	W2 = int(w2*rate)

	# Make sure we have a non-empty range
	if W1 <= 0:
		W1 = 1
	if W2 <= 0:
		W2 = 1

	# Get the subarrays leading up to a, b
	i1 = a-W1
	i2 = b-W2

	# This may happen if our start or stop are very close to the beginning of the file
	if i1 < 0:
		return 0, 0, np.inf

	if i2 <= 0:
		i2 = b-2

	x = x0[i1:a]
	y = x0[i2:b]

	# Only Joseph understands this arcane numpy magic
	delta = np.sum(y**2) - 2*np.correlate(x, y) + np.correlate(x**2, np.ones_like(y	))
	K = np.argmin(delta)
	# B = K + b
	A = K + a
	distance = delta[K]

	# TODO: Optional returns for modified A, B; unmodified a, b
	# return a, b, distance
	# return a, B, distance
	return a, b, distance

times = []

class LoopDialog(Toplevel):

	def StartPb(self, inctime=50, stopAt=None, lastTime=None):
		"""Start progress bar, incrementing every inctime ms, up to a bar value of stopAt"""
		self.pb.step()
		if lastTime == None:
			lastTime = time()
		else:
			nt = time()
			# print(nt-lastTime)
			times.append(nt - lastTime)
			lastTime = nt


		# print(int(self.pb['value']), stopAt)
		if int(self.pb['value']) == stopAt:
			self.pb.stop()
			print(sum(times)/len(times))
		else:
			self.after(inctime, self.StartPb, inctime, stopAt, lastTime)

	def LookForLoop(self, startTime:float,stopTime:float,path:str,stepTime:float,endTime:float,w1:float,w2:float,findStart:bool,findEnd:bool):
		"""startTime: Start of search range\n
		stopTime: End of search range\n
		path: Filepath sans extension\n
		stepTime: Search increment\n
		endTime: End of loop/file\n
		w1: Wiggle\n
		w2: Wiggle\n
		findStart:\n
		findEnd:"""
		
		assert findStart or findEnd
		
		# Minimum difference between loop start and loop end to analyze
		minimumTimeDifference = 1

		# sample rate and data
		x, rate = self.x, self.rate
		# Just look at one channel; it's probably enough
		x0 = x if x.ndim == 1 else x[:, 0]
		
		# endSample is the very end of  our file, (or otherwise the specified loop end-point)
		# while stopSample is the last in our search range. end-Entire, stop-Search.
		endSample	= int(endTime * rate)# if endTime else len(x)-1
		startSample	= int(startTime * rate)
		stopSample	= int(stopTime * rate)
		stepSample	= int(stepTime * rate)
		if stepSample == 0:
			stepSample = 1

		# The array of all times we want to check
		A = range(startSample, stopSample, stepSample)

		# We want to find the lowest distance; default to infinity
		dist = np.inf

		## Create a generator and length of generator for each situation
		if findStart and findEnd:
			# Check *every* pair of (a,b) in A
			pairs = itertools.product(A, A)
			maxI = len(A)**2

		elif findStart:
			# Check every pair of (a, end) in A
			pairs = ((a, endSample) for a in A)
			maxI = len(A)

		elif findEnd:
			# Check every pair of (start, b) in A
			pairs = ((startSample, b) for b in A)
			maxI = len(A)

		# Set up progress bar parameters
		self.pb.configure(maximum=100)
		logEvery = maxI/self.pb["maximum"]
		i=0
		self.pb.configure(mode="determinate")
		for a, b in pairs:

			# Update progress bar
			if i%logEvery < 1:
				self.pb.step()
				self.update()

			# Check if a and b are close together; lots of false positives
			if b - a < minimumTimeDifference * rate:
				continue
			a, b, d = improveloop(x0, a, b, rate, w1, w2)

			# Store the best results
			if d < dist:
				bestA = a
				bestB = b
				dist = d 

			i+=1
			if i == maxI:
				self.pb.stop()
				self.pb["value"] = 0
				# print(f"\nDone! Best A, B found to be: {bestA/rate} ({bestA}), {bestB/rate} ({bestB})")

		return bestA, bestB, rate

	def cb_Apply(self):
		bodyString = "Really apply?"
		icon = "question"
		# Check for edited loop end point, add warning
		if self.loopSlider.bars[1]["Value"] != self.loopSlider.init_lis[1]:
			bodyString += ("\n\nIf you used a loop end other than the end of the file, you will need to "
			"edit the file to end at the desired loop point. Audacity can do this, remember to write down the end time.")
			icon="warning"

		response = messagebox.askquestion(
			"Really apply?",
			bodyString,
			icon=icon)
		if response == YES:
			# Update the main window's loop point based on the bar
			self.originalStart.set(round(self.loopSlider.getValues()[0], 5))

	def cb_Close(self, e=None):
		response = messagebox.askquestion("Close loop config?", "Close loop config?", icon="warning")
		if response == YES:
			self.destroy()
			self.loopSlider.Unsubscribe(self)

	def cb_Find(self):
		# Big ass function
		a, b, rate = self.LookForLoop(
			self.searchSlider.getValues()[0],
			self.searchSlider.getValues()[1],
			self.path + FILETYPE,
			self.param_step.value.get(),
			self.loopSlider.getValues()[1],
			self.param_wiggle1.value.get(),
			self.param_wiggle2.value.get(),
			self.param_findStart.value.get(),
			self.param_findEnd.value.get()
		)
		if self.param_findStart.value.get():
			self.loopSlider.MoveLowest(a/rate)
		if self.param_findEnd.value.get():
			self.loopSlider.MoveHighest(b/rate)

	def cb_Preview(self):
		"""Play preview audio"""
		# Change our progress bar to a "working" mode
		self.pb.configure(mode="indeterminate")
		x, rate = self.x, self.rate
		
		# Time covered by half the screen width
		halftime = PREVIEW_HALFTIME * rate
		# Get start and end points as seconds, convert to samples
		start, end = self.loopSlider.getValues()
		start, end = int(rate * start), int(rate*end)
		loopDemoStart = x[end-halftime:end]
		loopDemoEnd = x[start:start+halftime]

		loopDemo = np.concatenate((loopDemoStart, loopDemoEnd))
		soundfile.write(PREVIEW_NAME, loopDemo, rate)
		
		
		# Stop the progress bar after playing our audio clip; seconds -> ms
		self.pb.configure(maximum=int(1000*PREVIEW_HALFTIME/UPDATE_TIME))
		
		r = winsound.PlaySound(PREVIEW_NAME, winsound.SND_ASYNC)

		# TODO: This is accurate (on my machine) on intervals of ~15.66ms (NOT QUITE 1/60s),
		# So a time of 31ms works pretty well. I don't know if that's universal.
		self.StartPb(UPDATE_TIME, self.pb["maximum"]*2)
		# self.after(int(PREVIEW_HALFTIME*2*1000), lambda: self.pb.stop())

	def cb_onArrow(self, delta):
		if self.last_changed_bar != None:
			self.last_changed_bar.KeyboardTweak(delta)

	def cb_onConfigure(self, e):
		for slider in [self.loopSlider, self.searchSlider]:
			windowSize = self.winfo_width()
			firstColumnSize = slider.winfo_x()
			windowSize = windowSize
			slider.ChangeWidth(windowSize - firstColumnSize)
		
		self.Update(True)

	def init_vars(self, path, startValue):
		self.oldMax = 0
		self.oldRate = 0
		self.oldPath = ""
		self.oldX = None

		
		# TEMP:
		# self.visualHalftime = 100

		self.path = path
		self.originalStart = startValue

	def init_binds(self):

		# Unsubscribe when we close
		self.protocol("WM_DELETE_WINDOW", self.cb_Close)
		self.bind("<Escape>", self.cb_Close)
		self.bind("<Left>", 		 lambda _: self.cb_onArrow(-0.00001))
		self.bind("<Right>", 		 lambda _: self.cb_onArrow( 0.00001))
		self.bind("<Control-Left>",  lambda _: self.cb_onArrow(-0.0001 ))
		self.bind("<Control-Right>", lambda _: self.cb_onArrow( 0.0001 ))
		self.bind("<Shift-Left>", 	 lambda _: self.cb_onArrow(-0.001  ))
		self.bind("<Shift-Right>", 	 lambda _: self.cb_onArrow( 0.001  ))
		
		# Page down
		self.bind("<Next>", lambda _: self.cb_onArrow( self.va.visualHalftime))
		# Page up
		self.bind("<Prior>",  lambda _: self.cb_onArrow(-self.va.visualHalftime))
		
		self.bind("<Configure>", self.cb_onConfigure)

	def Update(self, e=None):
		self.visualAid.Update(e)

	def __init__(self, master, path, startValue):
		super().__init__(master)

		self.init_vars(path, startValue)
		self.init_binds()
		self.title(path)

		try:
			self.x, self.rate = soundfile.read(self.path + FILETYPE)
			
		except RuntimeError as e:
			response = messagebox.showerror(
			"Could Not Read File",
			f"{e}\n\nWrong prefix, filename, or filetype (must be .ogg)?")
			self.destroy()
			return
		
		# Song data
		x = self.x
		rate = self.rate
		self.max = max(np.amax(x), abs(np.amin(x)))

		# Length of song in seconds
		maxTime = (len(x)-1) / rate

		Label(self, text="Loop").grid(row=0, column=0)
		self.loopSlider = MultiSlider(self, max_val=maxTime, init_lis=[startValue.get(), maxTime])
		self.loopSlider.Subscribe(self, self.Update)
		self.loopSlider.grid(row=0, column=1, columnspan=3, sticky="EW")

		Label(self, text="Search").grid(row=1, column=0)
		self.searchSlider = MultiSlider(self, max_val=maxTime, init_lis=[0, maxTime])
		self.searchSlider.grid(row=1, column=1, columnspan=3, sticky="EW")
		
		self.param_findStart = SongParameters.SP_Bool("", "Find start", value=True, maxSize=COL_1_CHAR_WIDTH)
		self.param_findEnd = SongParameters.SP_Bool("", "Find End", value=False, maxSize=COL_1_CHAR_WIDTH)
		self.param_step = SongParameters.SP_Float("", "Step", 0.01, maxSize=COL_1_CHAR_WIDTH)
		self.param_wiggle1 = SongParameters.SP_Float("", "Start Wiggle", 0.01, maxSize=COL_1_CHAR_WIDTH)
		self.param_wiggle2 = SongParameters.SP_Float("", "Stop Wiggle", 0.01, maxSize=COL_1_CHAR_WIDTH)

		self.param_findStart.MakeWidget(self).grid(sticky='EW', row=3, column=0, columnspan=2)
		self.param_findEnd.MakeWidget(self).grid(sticky='EW', row=4, column=0, columnspan=2)
		self.param_step.MakeWidget(self).grid(column=0, sticky='EW', columnspan=2)
		self.param_wiggle1.MakeWidget(self).grid(column=0, sticky='EW', columnspan=2)
		self.param_wiggle2.MakeWidget(self).grid(column=0, sticky='EW', columnspan=2)
		Button(self, text="Find", command=self.cb_Find).grid(row=8, column=0, sticky='W')
		Button(self, text="Preview", command=self.cb_Preview).grid(row=9, column=0, sticky='W')

		self.visualAid = WaveformDisplay(self)
		self.visualAid.grid(row=2,column=2, rowspan=9, columnspan=2, sticky='NEWS')
		# This allows the loop sliders and visual aid to stretch
		self.rowconfigure(2, weight=10)
		
		self.pb = Progressbar(self)
		self.pb.grid(row=10, column=0, columnspan=4, sticky='SEW')

		f3 = Frame(self)
		f3.grid(row=11, column=3, sticky='SE')
		self.rowconfigure(11, weight=1)
		Button(f3, text="Apply", command=self.cb_Apply).grid(row=0, column=0, sticky='SE')
		Button(f3, text="Close", command=self.cb_Close).grid(row=0, column=1, sticky='SE')

		
		self.grab_set()

if __name__ == "__main__":
	tik = Tk()
	iv = IntVar(tik)
	ld = LoopDialog(tik, "yrf", iv)
	ld.mainloop()