# Brief text tutorial
1. Extract the program to a reasonable location.
2. If creating a whole musicpack, create a new folder for that. I'll call this the workspace.
3. Create a copy of [DFCI Root Folder]\Bgm\bgm.txt and place it in the workspace; backup the original.
4. Convert or copy the songs you'd like to use, and place them into the workspace or a subfolder.
4b. Ensure that, relative to bgm.txt, the music filenames are no more than 32 characters. e.g.
        in the workspace, "My\Really\Cool\Music\Pack\caramelladansen.ogg" (45 chars) is no good.
        "MemeMusic\caramella.ogg" (23 chars) is fine.
    Also avoid spaces, using underscores or CamelCase instead, e.g.
        "Meme Music\Caramella Dansen.ogg" is no good.
        "Meme_Music\CaramellaDansen.ogg" is fine.
5. Open DFCIPE and click Open: find your copy of bgm.txt and select it.
6. Find the song that you would like to replace, and click File to select the new song.
7. If you already know where you want it to loop from, simply put in the loop point.
    It uses seconds, and up to 5 decimal places.
    If you don't know the loop details, see the Loop Guide for more instructions.
8. Click Save and test in game. You may want to adjust the volume. Volume ranges
    from 0 to 10,000. The developers recommended always keeping it at 10000 and
    adjusting the source. Since they recommended it, I would also recommend it.

# Loop Guide
Looping songs can be a tedious process; hopefully this tool helps.
First, a brief overview of the controls, top to bottom:

The loop bar determines where the loop will begin/resume from, and end at.
NOTE: the game *only* ends the loop at the end of the file; this bar if for
PE use only.

The search bar is the range that will be searched for the start, end, or both.

Find start: When checked, Find will search for the start of the loop within
the search range. Values are in seconds.

Find end: Same as above, but for the end. NOTE: Checking both boxes will result
in a *very* slow search, as every possible pair of points will be checked (it
squares the search space). Also, the search algorithm is made to find the start.
I haven't tested much for end, so I recommend choosing it manually for best results.

Step: How far to increment between every searched pair. Larger values are faster
but less accurate. I have found the sweet spot to be between 0.01 and 0.001.
Value is in seconds.

Wiggle: All samples (not based on step) leading up to the search points are
considered as loop candidates. Smaller values are faster but less accurate.
Note: If not equal, these values should be powers(?) of 10 of each other for
best performance, e.g. 0.1 and 0.01, or 0.01 and 0.01.
Value is in seconds.

Find: Perform the search.

Preview: Play to an 8 second clip (4 seconds on each side) to listen to the loop.

Waveform: In green (options soon), you see a section of the loop as determined
by the loop bar. The left half of the screen shows the loop END (that is, the
point later in the song). On the right is the loop START. A small time BEFORE
the END and AFTER the START are shown. 

In white, only on the left side, you see a small time BEFORE the START.
Ideally, this will line up perfectly with the green section on the left.
If you go down a section of a road twice, the lead up to that section
should be pretty similar both times, right?

At the top, in the center, the two times of the center point are shown.
On the sides, the time range before and after the center are shown in seconds.

See the Hotkeys section for keyboard controls for the loop slider and display.

Progress bar: Shows a rough estimation of preview and search progress.

Apply: Apply the loop start back to the main window.

Cancel: Close the window, not saving anything.

Here's my process:
1. Identify the first and last chorus (or other repeating section) of the song.
    I just skip through the song on with a regular media player for this.
2. Set the end point a short time, one or two bars, into the later chorus.
3. Set the search range around the earlier chorus. Find. Preview.
    It will probably be a bit off.
4. Using the keyboard, adjust the loop start. Begin with the waveform zoomed
    out, looking for similar shapes. Try to match up peaks, especially the
    wider, rolling hills that represent the bass frequencies. Preview, and
    make sure that you're at an okay chorus on both sides. Sometimes the
    chorus will have different instrumentation the last time it plays.
    You may need to move the end to a different chorus, or the song may simply
    not loop cleanly without further editing.
5. A couple of hints on knowing the direction to adjust in:
    If you hear repeated sounds (words, syllables... whatever instruments do),
    then your start is too early: move it forward. If you hear skipped sounds,
    your start is too late: move it back.
6. Zoom in and adjust again, looking for similar shapes. To reduce pop, try to
    to get the slope of the two green halves to match at the intersection.
    This should also line the left green half up with the white half.

I've done a few custom songs before DFCI, but generally I don't have much
experience. I would appreciate tips, or feature suggestions for this window.
You can find me @Maqo on the DFCI Discords.

# FAQ
- Q: What is the DFCI Root folder?
A: The one with ringgame.exe.

- What is the "global prefix"?
A: When the game looks for a song, this is the starting folder, and all other folders
are relative to it. If you're working directly on the game files (consider making backups),
this will be: [DFCI Root folder]\Bgm
In other words, it's the path to the folder of your bgm.txt file.
This path is determined automatically when opening a bgm.txt file.
It is not saved anywhere or used in game.

- Q: Where is the round 3 music?
A: Under the SPECIAL tab, named stage_stagef_id.

- Q: What are the STAGE, TITLE, STORY, etc tabs?
These are somewhat arbitrary categories that I made myself based on the order
of the songs in the original config file.
For the most part, you'll want to change stuff in STAGE. 
You may also want to change sys_* under SCENE: this has music for character select, victory, etc.
The special music for round 3 is under the SPECIAL tab, named stage_stagef_id.

- Q: In game, it skips the loop end and goes to the end of the file. What's with that?
A: The game simply doesn't support looping at any point other than the end.
Edit your file.

- Q: The music doesn't play at all in game?
A: Make sure that the file is a stereo, ogg vorbis file with no embedded thumbnail.
I don't know all of the limitations, but the most common cause in my experience is
that the songs are mono, or came directly from an album and had image data.
Audacity can easily fix these issues.
You may also be over the 32 character limit, or your prefix may be set wrong; see guide 4b.

- Q: I can't load a song in the loop editor?
A: Make sure that it's an ogg file without image or lyric data (try loading and 
exporting it through Audacity or ffmpeg), and that your prefix is set correctly.

- Q: What does the "Prevent Recording" option do?
A: This is an option for PS4 (and 5?). It's used to prevent recording or audio
streaming on certain screens. It should have no affect on the PS3, arcade, or arcade+ versions.

# Hotkeys
## Main window
Save bgm.txt as - Alt A
Save a bgm.txt  - Alt S
Open a bgm.txt  - Alt O
Change root folder - Alt R
Open help links - Alt H

## Loop window
Close loop window - Escape

(After hovering over a slider)
fine move left		- Left
fine move right		- Right
regular move left	- Ctrl Left
regular move right	- Ctrl Right
coarse move left	- Shift Left
coarse move right	- Shift Right
page move left		- Page up
page move right		- Page down

Zoom time out   - Alt left
Zoom time in    - Alt right
Zoom height out - Down
Zoom height in  - Up