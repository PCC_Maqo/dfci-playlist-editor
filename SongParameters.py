# Python representations of bgm.txt song parameters

import os
import tkinter
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *

import Loop
import Validation

prefix = os.path.curdir
tk = None
bgs = 0

class SongParameter():
    def __init__(self, name, friendlyName, value, varType, description, maxSize=None):
        self.name = name
        self.friendlyName = friendlyName
        self.value = varType(value=value)
        self.maxSize = maxSize

    def MakeWidget(self, parent):
        global bgs
        f = Frame(parent)
        bgs+=1
        if self.maxSize:
            Label(f, anchor='w', text=self.friendlyName, width=self.maxSize).grid(column=0, sticky='EW')
        else:
            Label(f, anchor='w', text=self.friendlyName).grid(column=0, sticky='EW')
        return f

class SP_String(SongParameter):
    def __init__(self, name, friendlyName, value="", description="", maxSize=None):
        super().__init__(name, friendlyName, value, StringVar, description, maxSize)
        self.Update(value)

    def Update(self, value):
        if value:
            self.value.set(value)

    def cb_filename(self):
        path = filedialog.askopenfilename(
                initialdir=prefix,
                title="Select DFCI 'Bgm' File",
                filetypes=[("ogg files", "*.ogg"), ("all files", "*.*")]
            )
        if not path:
            return False
        
        if not path.lower().endswith(".ogg"):
            # TODO: Alert popup
            return False
        
        if prefix:
            path = os.path.relpath(path, prefix)

        self.value.set(path[:-4])

        pass

    def MakeWidget(self, parent):
        frame = super().MakeWidget(parent)
        e = Entry(frame, textvariable=self.value, width=32)
        butt = Button(frame, text="File", width = 5, command=self.cb_filename)
        butt.grid(column=2,row=0, sticky='E', padx=2, pady=1)
        e.grid(column=1,row=0, sticky='E', padx=2, pady=1)

        vcmd = (e.register(Validation.val_StringLimit), 
            "%P")
        e.configure(validate="key", validatecommand=vcmd)

        return frame

class SP_Bool(SongParameter):
    def __init__(self, name, friendlyName, value=False, description="", maxSize=None):
        super().__init__(name, friendlyName, int(value), BooleanVar, description, maxSize)
        self.Update(value)
    
    def Update(self, value):
        if value:
            self.value.set(int(value))

    def MakeWidget(self, parent):
        frame = super().MakeWidget(parent)
        # self.boolvar = IntVar(value=self.value)
        cb = Checkbutton(frame, variable=self.value)
        # cb.pack(side=RIGHT)
        cb.grid(row=0, column=1, sticky='W', padx=2, pady=1)
        frame.grid_columnconfigure(1, weight=1)
        return frame

class SP_Float(SongParameter):
    def __init__(self, name, friendlyName, value=0.0, description="", maxSize=None):
        super().__init__(name, friendlyName, value, DoubleVar, description, maxSize)
        self.Update(value)
    
    def Update(self, value):
        if value:
            self.value.set(float(value))

    def MakeWidget(self, parent):
        frame = super().MakeWidget(parent)
        sb = Spinbox(frame, from_=0, to=180, increment = 0.00001, width=12, textvariable=self.value)
        vcmd = (sb.register(Validation.val_Float), 
            "%P")
        sb.configure(validate="key", validatecommand=vcmd)
        # sb.pack(side=RIGHT)
        sb.grid(row=0, column=1, sticky='W', padx=2, pady=1)
        # frame.grid_columnconfigure(0, weight=1)
        frame.grid_columnconfigure(1, weight=1)
        return frame

class SP_Loop(SP_Float):
    """
    name: Name of variable to write to config file\n
    friendlyName: Human readable name\n
    songPath: StringVar with song location\n
    value: Loop start position in seconds with decimals\n
    description: Tooltip displayed on hover
    """
    def __init__(self, name, friendlyName, songPath, value=0.0, description="", maxSize=None):
        super().__init__(name, friendlyName, value, description, maxSize)
        self.songPath = songPath
    
    def cb_Autoloop(self):
        Loop.LoopDialog(tk, prefix + "/" + self.songPath.get(), self.value)

    def MakeWidget(self, parent):
        frame = super().MakeWidget(parent)
        butt = Button(frame, text="Auto", width=5, command=self.cb_Autoloop)
        butt.grid(row=0,column=2, sticky="W", padx=2, pady=1)
        return frame

class SP_Int(SongParameter):
    def __init__(self, name, friendlyName, value=0, description="", maxSize=None):
        super().__init__(name, friendlyName, value, StringVar, description, maxSize)
        self.Update(value)
    
    def Update(self, value):
        if value:
            self.value.set(str(value))

    def MakeWidget(self, parent):
        frame = super().MakeWidget(parent)
        sb = Spinbox(frame, from_=0, to=10000, increment=1, format="%5.0f", width=6, textvariable=self.value)
        vcmd = (sb.register(Validation.val_IntRange), 
            "%P")
        sb.configure(validate="key", validatecommand=vcmd)
        # sb.pack()
        sb.grid(row=0, column=1, sticky='W', padx=2, pady=1)
        frame.grid_columnconfigure(1, weight=1)
        return frame

if __name__ == "__main__":
    import BigBeats
    BigBeats.main()
