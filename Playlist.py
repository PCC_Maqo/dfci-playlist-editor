from enum import IntEnum

from SongParameters import SP_Bool, SP_Float, SP_Int, SP_Loop, SP_String

## This will be a big ass dictionary of all of the song filenames -> readable names, compiled manually. 
# Not every song needs a readable name (e.g. story stuff can be skipped); default to input
songToFriendlyDict = {

}

musicTypes = [
    "STAGE"
    ,"TITLE"
    ,"STORY"
    ,"FINAL"
    ,"SCENE"
    ,"SPECIAL"
    ,"EXTRA"
    # ,"ENDING"
]
class MusicType (IntEnum):
    STAGE = 1 #                                  //戦闘中キャラクター固有ＢＧＭ
    TITLE = 2 #                                  // タイトル曲(MUSIC用)
    STORY = 3 #                                  //ストーリー関係
    FINAL = 4 # Special battle music with vocals // 決戦ＢＧＭ（ボーカルあり）
    SCENE = 5 # System                           //シーンＢＧＭ
    SPECIAL = 6 # Special battle music no vocals //戦闘中特殊ＢＧＭ(ボーカル無し)
    EXTRA = 7 # Sound test                       //主題歌(Musicで聞く用)
    # ENDING= 8 #                                  //エンディングＢＧＭ
    #SCENE2= 9 # System2                          //シーンＢＧＭ
    
# Based on developer comments in the original file
def SongNumberToType(x):
    if x <= 29:
        return MusicType.STAGE
    elif x <= 32:
        return MusicType.TITLE
    elif x <= 70:
        return MusicType.STORY
    elif x <= 73:
        return MusicType.FINAL
    elif x <= 89:
        return MusicType.SCENE
    elif x <= 92:
        return MusicType.SPECIAL
    elif x <= 94:
        return MusicType.EXTRA
    # elif x <= 94:
        # return MusicType.ENDING
    elif x <= 99:
        return MusicType.SCENE

class Song:
    def __init__(self, number):
        # Used by RingGame.exe
        self.number = number #BGM_000
        self.File = SP_String("File", "Filename", description="Without ogg extension", maxSize=len("Prevent Recording"))
        self.IsLoop = SP_Bool("IsLoop", "Loop", True, maxSize=len("Prevent Recording"))
        self.LoopPos = SP_Loop("LoopPos", friendlyName="Loop at", songPath=self.File.value, value=0, description="Seconds with decimal", maxSize=len("Prevent Recording"))
        self.Volume = SP_Int("Volume", "Volume", 10_000, description="Out of 10000", maxSize=len("Prevent Recording"))
        self.norecording = SP_Bool("norecording", "Prevent Recording", False, maxSize=len("Prevent Recording"))

        # Only used by BigBeats
        self.description = "name or description of song"
        self.musicType = SongNumberToType(number)

    def UpdateDict(self, d):
        self.File.Update(d["File"].strip())
        self.IsLoop.Update(d["IsLoop"])
        self.LoopPos.Update(d["LoopPos"])
        self.Volume.Update(d["Volume"])
        self.norecording.Update(d["norecording"])
    
    @property
    def parameterNames(self):
        return [x.name for x in self.parameters]

    @property
    def parameters(self):
        return [self.File,self.IsLoop,self.LoopPos,self.Volume,self.norecording]

    def __repr__(self):
        s = f"[BGM_{self.number:03}]\n"
        s += "File = " + self.File.value.get().replace("/", "\\") + "\n"
        s += "IsLoop = " + str(int(self.IsLoop.value.get())) + "\n"
        s += "LoopPos = " + str(self.LoopPos.value.get()) + "\n"
        s += "Volume = " + str(self.Volume.value.get()) + "\n"
        s += "norecording = " + str(int(self.norecording.value.get())) + "\n"
        s += "\n"
        return s
    
def SaveSongs(path, songs):
    """Create a bgm.txt file at path and save the list of songs. Mind the encoding."""
    if not path:
        return
    f = open(path, 'w', encoding="shift_jis")
    # File is padded at the start with a newline, for whatever reason
    f.write("\n")
    for song in songs:
        f.write(str(song))
        f.write("\n")
    f.write("\n")
    f.close()

def GenerateSongs(path, n=-1):
    """Read a bgm.txt file at path and return a list of songs. Mind the encoding."""
    f = open(path, 'r', encoding="shift_jis")
    songs = []
    sections = 0
    while line := f.readline():
        if n > 0 and len(songs) >= n:
            f.close()
            return songs
        # Ignore comments
        if line[:2] == "//":
            if len(songs) != 0:
                # print(f"End section on song {songs[-1].number}")
                pass
            sections+=1
            # print(f"Comment section {sections}", line[:-1])
            continue

        # Find the first track
        if line[:5] != "[BGM_":
            continue
        
        
        number = int(line[5:5+3])
        song = Song(number)

        d={  "File" : None
            ,"IsLoop" : None
            ,"LoopPos" : None
            ,"Volume" : None
            ,"norecording" : None
        }
        while (line:=f.readline()).strip() != "":

            for var in d.keys():
                if line.startswith(var):
                    end = line.find("//")
                    d[var] = line[line.find("=")+2 : end]
                    break

        song.UpdateDict(d)
        songs.append(song)
    return songs
