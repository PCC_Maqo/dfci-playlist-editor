# %d = Type of action (1=insert, 0=delete, -1 for others)
# %i = index of char string to be inserted/deleted, or -1
# %P = value of the entry if the edit is allowed
# %s = value of entry prior to editing
# %S = the text string being inserted or deleted, if any
# %v = the type of validation that is currently set
# %V = the type of validation that triggered the callback
#      (key, focusin, focusout, forced)
# %W = the tk name of the widget

def val_None(s):
    return False

def val_StringLimit(s, limit=32):
    # print("s is", s)
    if len(s) > limit:
        return False
    return True

# TODO: Strip leading 0's?
def val_IntRange(s, lower=0, upper=10000):
    try:
        d = int(s)
        if d < lower or d > upper:
            return False
    except:
        return False
    return True

# TODO: Strip leading 0's?
def val_Float(s, lower=0, upper=3600):
    try:
        f = float(s)
        if f < lower or f > upper:
            return False
    except:
        return False
    return True